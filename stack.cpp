#include "stack.h"
#include <iostream>
#include "linkedList.h"


void initStack(stack* s)
{
	numNode* temp = new numNode;
	temp = s->lst-> head;
	if (!temp)
	{
		s->lst->head->number = NULL;
	}
	while (temp != NULL)
	{
		temp->number = NULL;
		temp = temp->next;
	}
	delete temp;
}

/*
adding a node to the list (stuck)
*/
void push(stack* s, unsigned int element)
{
	addNumToList(s->lst, initNumNode(element));
}

/*
removing a node from the stack
*/
int pop(stack* s)
{
	return subFromList(s->lst);
}


/*
cleaning the stack
*/
void cleanStack(stack* s)
{
	deleteList(s->lst);
}