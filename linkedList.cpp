#include "linkedList.h"
#include <iostream>

numNode* initNumNode(unsigned int numberToAdd)
{
	numNode* num = new numNode;
	num->number = numberToAdd;
	num->next = NULL;
	return num;
}

/*
a function to mange the list 
*/
void mangeList()
{
	LIST list = { NULL };
	int i = 0;
	unsigned int number = 0;
	for (i = 0; i < 5; i++) 
	{
		std::cout << "enter a number" << std::endl;
		std::cin >> number;
		getchar();
		addNumToList(&list, initNumNode(number));
		printf(" the number entered : %d\n", list.head->number);
	}
	subFromList(&list);
	printf(" the before last entered : %d\n", list.head->number);
	getchar();
	deleteList(&list);
	
}

/*
deleting the list
*/
void deleteList(LIST* list)
{
	std::cout << "deleting the list" << std::endl;
	while (list->head != NULL)
	{
		subFromList(list); 
	}
}

/*
adding a numNode to the list
*/
void addNumToList(LIST* list, numNode* nodeToAdd)
{
	if (list ->head == NULL)
	{
		list->head = nodeToAdd; // if the list is empty
		
	}
	else 
	{
		nodeToAdd->next = list->head; 
		list->head  = nodeToAdd;
	}
}

/*
removing a node from the list
*/
int subFromList(LIST* list)
{
	numNode* nextHead = new numNode;
	numNode* oldHead = new numNode;
	int numberReleased = EMPTY;
	if (list == NULL)
	{
		std::cout << "the list is empty" << std::endl; // telling the user that his list is empty
	}
	else
	{
		nextHead = list->head->next;
		oldHead = list->head;
		list->head = nextHead;
		numberReleased = oldHead->number;
		delete[] oldHead;
	}
	return numberReleased; // fixed for the pop to avoid coping my code
}