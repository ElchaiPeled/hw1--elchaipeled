#include "queue.h"
#include <iostream>

/*
init the queue
*/
void initQueue(queue* q, unsigned int size)
{
	int i = 0;
	q->inLine = new int [size];
	q->maxSize = size;
	for (i = 0; i < size; i++)
	{
		q->inLine[i] = EMPTY;
	}
}

/*
enter -1 to the line
*/
void cleanQueue(queue* q)
{
	int i = 0;
	for (i = 0; i < q->maxSize; i++)
	{
		q->inLine[i] = EMPTY;
	}
	q->countEnter = 0;
	q->countOut = 0;
	//delete[] q ->inLine; // to free the array because i didnt understand where i should put the delete
}
/*
entering one by one numbers to the line
*/
void enqueue(queue* q, unsigned int newValue)
{
	if (q->countEnter != q->maxSize)
	{
		q->inLine[q->countEnter] = newValue; // in the main i said that all the strcut is 0 (the counters also)
		q->countEnter++;
	}
}

/*
letting everyone in the line to go one at a time (realesing one member of the array at a time)
*/
int dequeue(queue* q)
{
	int numInLine = 0;
	numInLine = q->inLine[q->countOut];
	q->inLine[q->countOut] = EMPTY;
	if (q-> countOut < q->maxSize)
	{
		q->countOut++;
	}
	else
	{
		q->countOut = q->countEnter;
	}
	q->countEnter--;
	return numInLine;
}
