#ifndef LINKEDLIST_H
#define LINKEDLIST_H
#define EMPTY -1
typedef struct numNode
{
	unsigned int number;
	struct numNode* next;
}numNode;


typedef struct LIST
{
	struct numNode* head;
}LIST;


numNode* initNumNode(unsigned int numberToAdd);
void addNumToList(LIST* list, numNode* nodeToAdd);

int subFromList(LIST* list);
void mangeList();
void deleteList(LIST* list);

#endif